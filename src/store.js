import Vue from 'vue'
import Vuex from 'vuex'
import db from '@/firebase/db'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    query: null,
    hooks: [],
    comments: []
  },
  getters: {
    query (state) {
      return state.query
    },
    hooks (state) {
      return state.hooks
    },
    comments (state) {
      return state.comments.sort((a, b) => {
        const aDate = new Date(a.createdAt.seconds * 1000)
        const bDate = new Date(b.createdAt.seconds * 1000)

        return aDate - bDate
      })
    }
  },
  mutations: {
    setQuery (state, payload) {
      state.query = payload
    },
    setComments (state, payload) {
      state.comments = payload
    },
    addComment (state, payload) {
      state.comments.push(payload)
    },
    setHooks (state, payload) {
      state.hooks = payload
    }
  },
  actions: {
    setQuery ({ commit }, payload) {
      commit('setQuery', payload)
    },
    getComments ({ commit, state }) {
      return db.collection('comments').where('accountId', '==', state.query.accountId).where('threadId', '==', state.query.threadId)
        .onSnapshot((querySnapshot) => {
          const comments = querySnapshot.docs.map((doc) => ({
            ...doc.data(),
            id: doc.id
          }))
          commit('setComments', comments)
        })
    },
    addComment ({ dispatch }, payload) {
      return db.collection('comments').add(payload)
        .then(() => dispatch('getComments'))
    },
    getHooks ({ commit, state }) {
      db.collection('webhooks').where('accountId', '==', state.query.accountId).get()
        .then((res) => {
          const hooks = res.docs.map((doc) => ({
            ...doc.data(),
            id: doc.id
          }))
          commit('setHooks', hooks)
        })
    },
    updateHook ({ commit }, payload) {
      const ref = db.collection("hooks").doc(payload.id)

      ref.update({
        hits: payload.hits + 1
      }).then(() => commit('getHooks'))
    }
  }
})
