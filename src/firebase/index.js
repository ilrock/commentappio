import firebase from '@firebase/app'
import keys from '@/keys'
import '@firebase/auth'
import '@firebase/firestore'

if (process.env.NODE_ENV === 'development') {
  firebase.initializeApp({
    apiKey: process.env.VUE_APP_API_KEY,
    authDomain: process.env.VUE_APP_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_DB_URL,
    projectId: process.env.VUE_APP_PROJECT_ID,
    storageBucket: process.env.VUE_APP_TORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_SENDER_ID
  })
} else {
  firebase.initializeApp({
    apiKey: keys.apiKey,
    authDomain: keys.authDomain,
    databaseURL: keys.databaseURL,
    projectId: keys.projectId,
    storageBucket: keys.storageBucket,
    messagingSenderId: keys.messagingSenderId
  })
}

export default firebase
